"  Key mappings
map <Space> <Leader>
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>

" `SPC l s` - save current session
nnoremap <leader>ls :SSave<CR>

" `SPC l l` - list sessions / switch to different project
nnoremap <leader>ll :SClose<CR>

" Go to end of line by the use of L
map L $
map H ^


inoremap kj <ESC>

nnoremap <leader>f :Files<CR>
