;;; -*- no-byte-compile: t -*-
(define-package "zoom" "20190523.1300" "Fixed and automatic balanced window layout" '((emacs "24.4")) :commit "578295532fb1c4ad2a2e95894e65cce02f812b54" :keywords '("frames") :authors '(("Andrea Cardaci" . "cyrus.and@gmail.com")) :maintainer '("Andrea Cardaci" . "cyrus.and@gmail.com") :url "https://github.com/cyrus-and/zoom")
