;;; -*- no-byte-compile: t -*-
(define-package "ox-reveal" "20190616.1545" "reveal.js Presentation Back-End for Org Export Engine" '((org "8.3")) :commit "621bde6fb394b97af58f0d8d2075a7056d5a022d" :keywords '("outlines" "hypermedia" "slideshow" "presentation") :authors '(("Yujie Wen <yjwen.ty at gmail dot com>")) :maintainer '("Yujie Wen <yjwen.ty at gmail dot com>"))
