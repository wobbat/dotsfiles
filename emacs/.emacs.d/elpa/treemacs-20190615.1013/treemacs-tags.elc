;ELC   
;;; Compiled
;;; in Emacs version 26.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\210\300\307!\210\300\310!\210\300\311!\210\300\312!\210\300\313!\210\300\314!\210\300\315!\210\300\316!\207" [require xref imenu dash f treemacs-core-utils treemacs-rendering treemacs-customization treemacs-faces treemacs-visuals treemacs-dom treemacs-icons inline cl-lib treemacs-macros] 2)
#@269 Return the path of tag labels leading to BTN.

The car of the returned list is the label of BTN while its cdr is the top down
path starting at the absolute path of the file the tags belong to.

These paths are used to give tag nodes a unique key in the dom.

(fn BTN)
(defalias 'treemacs--tags-path-of #[257 "\300\301\"\211\203\f \211\202\\ \302\303T\304\"\206 e\305\304\"\206 d\"\300\306\"\307\203O \300\301\"\204O \302\303T\304\"\2069 e\305\304\"\206A d\"B\262\300\306\"\262\202$ \300\301\"B\262B\266\203\207" [get-text-property :path buffer-substring-no-properties previous-single-property-change button next-single-property-change :parent nil] 10 (#$ . 788)])
(byte-code "\300\301\302\303#\300\301\304\305#\300\207" [function-put treemacs--tags-path-of compiler-macro treemacs--tags-path-of--inliner side-effect-free t] 5)
#@25 

(fn INLINE--FORM BTN)
(defalias 'treemacs--tags-path-of--inliner #[514 "\3002e \211\301!\203 \211\202 \302\303!\304\305\306\307ED\305\310\311\312DD\313\306\314ED\315CE\316\317\313\320\306\313\307EDE\321\312\313D\315E\322\313\306\313\314EEF\321\306\313\307E\315E\323\311\315E\257F=\203[ \211\202b \324DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "btn" -if-let path treemacs-button-get :path let lbl treemacs--get-label-of node :parent ret while and null push setq cons macroexp-let*] 17 (#$ . 1640)])
#@80 Put top level leaf nodes in INDEX under DEFAULT-NAME.

(fn INDEX DEFAULT-NAME)
(defalias 'treemacs--partition-imenu-index #[514 "\300\211\203( \211@\301!\203 \211B\262A\262\202$ BB\262\300\262\210\202 \237\207" [nil imenu--subalist-p] 7 (#$ . 2186)])
(byte-code "\300\301\302\303#\300\301\304\303#\300\207" [function-put treemacs--partition-imenu-index pure t side-effect-free] 5)
#@206 Non-nil if MODE is derived from one of MODES.
Uses the `derived-mode-parent' property of the symbol to trace backwards.
If you just want to check `major-mode', use `derived-mode-p'.

(fn MODE &rest MODES)
(defalias 'treemacs--provided-mode-derived-p #[385 ">\204 \300N\211\262\204  \207" [derived-mode-parent] 4 (#$ . 2588)])
#@203 Post process a tags INDEX for the major INDEX-MODE the tags were gathered in.
As of now this only decides which (if any) section name the top level leaves
should be placed under.

(fn INDEX INDEX-MODE)
(defalias 'treemacs--post-process-index #[514 "\211\300>\203 \207\301\302\"\203 \303\304\"\207\303\305\"\207" [(python-mode org-mode markdown-mode) treemacs--provided-mode-derived-p conf-mode treemacs--partition-imenu-index "Sections" "Functions"] 5 (#$ . 2927)])
(byte-code "\300\301\302\303#\300\301\304\303#\300\207" [function-put treemacs--post-process-index pure t side-effect-free] 5)
#@39 Fetch imenu index of FILE.

(fn FILE)
(defalias 'treemacs--get-imenu-index #[257 "\305\211\211\306!\211\203 \211\262\202- \307K\310K\311\312\313\314\315!\316\"\317$\216\310M\210\320!\262)\266\3211\213 \3221\204 \323!\205 rq\210\324=\203K \325\302!\210	\n\204S \205V \326\327!\262\262)\211\204d \330!\210\205 @@\331\230\203t A\262\332\232?\205 \333\"00\202\240 0\305\262\202\240 \305\f\204\236 \334\335\336\337\340\341#\342\343\"#\210\262\207" [major-mode treemacs-elisp-imenu-expression imenu-generic-expression imenu-create-index-function treemacs--no-messages nil get-file-buffer ignore run-mode-hooks make-byte-code 0 "\301\300M\207" vconcat vector [run-mode-hooks] 2 find-file-noselect (error) (imenu-unavailable) buffer-live-p emacs-lisp-mode make-local-variable imenu--make-index-alist t kill-buffer "*Rescan*" (nil) treemacs--post-process-index message "%s %s" propertize "[Treemacs]" face font-lock-keyword-face format "Encountered error while following tag at point: %s"] 13 (#$ . 3532)])
#@210 Return the text to insert for a tag leaf ITEM.
Use PREFIX for indentation.
Set PARENT and DEPTH button properties.
ITEM: String . Marker
PREFIX: String
PARENT: Button
DEPTH: Int

(fn ITEM PREFIX PARENT DEPTH)
(defalias 'treemacs--insert-tag-leaf #[1028 "\300@\301\302\303\304\305\306\307\310\311\312\313\314\315A&D\207" [propertize button (t) category default-button face treemacs-tags-face help-echo nil :state tag-node :parent :depth :marker] 23 (#$ . 4569)])
(byte-code "\300\301\302\303#\300\207" [function-put treemacs--insert-tag-leaf compiler-macro treemacs--insert-tag-leaf--inliner] 4)
#@46 

(fn INLINE--FORM ITEM PREFIX PARENT DEPTH)
(defalias 'treemacs--insert-tag-leaf--inliner #[1285 "\3002\272 \301!\203 \211\202 \302\303!\301!\203 \211\202  \302\304!\301!\203+ \211\202. \302\305!\301!\203: \211\202= \302\306!\307\310\311\nD\312\313D\312\314CD\312\315D\312\316D\312\317D\312\320D\312\321D\322\323\312\324D\325\326\327\330D\257E=\203w \211\202~ \331DC\"\266\203=\203\212 \211\202\221 \331DC\"\266\203=\203\235 \211\202\244 \331DC\"\266\203=\203\260 \211\202\267 \331DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "item" "prefix" "parent" "depth" list propertize car quote button t category default-button face treemacs-tags-face help-echo nil :state tag-node :parent :depth :marker cdr macroexp-let*] 34 (#$ . 5179)])
#@225 Return the text to insert for a tag NODE.
Use PREFIX for indentation.
Set PARENT and DEPTH button properties.

NODE: String & List of (String . Marker)
PREFIX: String
PARENT: Button
DEPTH: Int

(fn NODE PREFIX PARENT DEPTH)
(defalias 'treemacs--insert-tag-node #[1028 "\300@\301\302\303\304\305\306\307\310\311\312\313\314\315A&D\207" [propertize button (t) category default-button face treemacs-tags-face help-echo nil :state tag-node-closed :parent :depth :index] 23 (#$ . 5983)])
(byte-code "\300\301\302\303#\300\207" [function-put treemacs--insert-tag-node compiler-macro treemacs--insert-tag-node--inliner] 4)
#@46 

(fn INLINE--FORM NODE PREFIX PARENT DEPTH)
(defalias 'treemacs--insert-tag-node--inliner #[1285 "\3002\272 \301!\203 \211\202 \302\303!\301!\203 \211\202  \302\304!\301!\203+ \211\202. \302\305!\301!\203: \211\202= \302\306!\307\310\311\nD\312\313D\312\314CD\312\315D\312\316D\312\317D\312\320D\312\321D\322\323\312\324D\325\326\327\330D\257E=\203w \211\202~ \331DC\"\266\203=\203\212 \211\202\221 \331DC\"\266\203=\203\235 \211\202\244 \331DC\"\266\203=\203\260 \211\202\267 \331DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "node" "prefix" "parent" "depth" list propertize car quote button t category default-button face treemacs-tags-face help-echo nil :state tag-node-closed :parent :depth :index cdr macroexp-let*] 34 (#$ . 6614)])
#@123 Open tag items for file BTN.
Recursively open all tags below BTN when RECURSIVE is non-nil.

(fn BTN &optional RECURSIVE)
(defalias 'treemacs--expand-file-node #[513 "\306\307\"\310!\211\203\300\212`\311\312\313T\314\"\206 e\315\314\"\206# d\316\317$\210\315\314\"\206/ db\210\320\321\306\322\"T\323	GY\204O \n@=\203O \nA\f=\204S \324!\210	HP@\311P>P\203\317 	\211\203\316 \211@\325!\203\221 \326@\314\327\330\331\332\333\334\311\316\335\336\322\337A&D\202\254 \326@\314\340\330\331\332\333\334\311\316\341\336\322\342A&D\343\203\305 @\211B\262\210\211T\262A\262\202\255 \266A\266\202\202g \210\237\266\206\"c\210\344\306\336\"\211\203\352 \306\307\"\202u\306	\307\"\211\211;\203#\211\345\230\262\203\202q\346!\211G\347V\203\211GSH\350=\203\211\343\351O\202\211\262\202q\211@\352=\2037G\353V\205q\354!\202q\211@;\203OG\353V\203J\354!\202q@\202q\355@!?>\203mG\353V\203f\354!\202q@\353H\202q\356\357\"\262\262\262#\210\360!\210\311\210\203\265\361!\343\203\263@\306\316\"\335=\203\247\313T\314\"\206\240eb\210\362\363\"\210\210\211T\262A\262\202\210\266)\364`\"\262)\202\352\365\326\332\366#D\211\203\340@\204\340\367\370\326\371\332\372#\373\320\373\"!#\210A\205\350\374\375!\262\262\207" [buffer-read-only treemacs--indentation-string-cache treemacs--indentation-string-cache-key treemacs-indentation treemacs-indentation-string treemacs-icon-tag-node-closed get-text-property :path treemacs--get-imenu-index nil put-text-property previous-single-property-change button next-single-property-change :state file-node-open apply concat :depth "\n" treemacs--build-indentation-cache imenu--subalist-p propertize (t) category default-button face treemacs-tags-face help-echo tag-node-closed :parent :index (t) tag-node :marker 0 treemacs-on-expand "/" file-name-directory 1 47 -1 :custom 2 butlast type-of error "Path type did not match: %S" treemacs--reopen-tags-under treemacs--get-children-of treemacs--expand-tag-node t count-lines "No tags found for %s" font-lock-string-face message "%s %s" "[Treemacs]" font-lock-keyword-face format treemacs--do-pulse treemacs-on-failure-pulse-face treemacs-icon-tag-leaf cl-struct-treemacs-project-tags treemacs--no-messages treemacs-pulse-on-failure] 34 (#$ . 7424)])
#@110 Close node given by BTN.
Remove all open tag entries under BTN when RECURSIVE.

(fn BTN &optional RECURSIVE)
(defalias 'treemacs--collapse-file-node #[513 "\212\301\302\303T\304\"\206 e\305\304\"\206 d\306\307$\210\310\311 !\211\2034 \312\313\"T\314\315\"\312\313\"\262U\204: \316 \210\202\227 \305\304\"\206B d\205u \312\313\"\310\305\304\"\206T d\315\"\211\203s \312\313\"W\203s \310\305\304\"\206l d\315\"\262\202V \262\211\203\221 \317\303T\304\"\206\203 e!\305\304\"\206\214 d\262\202\222 d|\266\210\320\312\321\"\"*\207" [buffer-read-only nil put-text-property previous-single-property-change button next-single-property-change :state file-node-closed next-button point-at-eol get-text-property :depth copy-marker t delete-trailing-whitespace previous-button treemacs-on-collapse :path] 10 (#$ . 9771)])
#@802 Visit tag section BTN if possible, expand or collapse it otherwise.
Pass prefix ARG on to either visit or toggle action.

FIND-WINDOW is a special provision depending on this function's invocation
context and decides whether to find the window to display in (if the tag is
visited instead of the node being expanded).

On the one hand it can be called based on `treemacs-RET-actions-config' (or
TAB). The functions in these configs  are expected to find the windows they need
to display in themselves, so FIND-WINDOW must be t. On the other hand this
function is also called from the top level vist-node functions like
`treemacs-visit-node-vertical-split' which delegates to the
`treemacs--execute-button-action' macro which includes the determination of
the display window.

(fn BTN ARG FIND-WINDOW)
(defalias 'treemacs--visit-or-expand/collapse-tag-node #[771 "\302!\303!\211\304\267\202\237\305\306\"@\211@A\307\310\"\204% \311\"\202\363\203L \211\2051 \312!\262\211\205: \313!\262\211\203G \314!\210\202K \315\316!\210\210\317!\210\320\321!!\203] \211b\202\363\322r\321	!q\210\305	\323\"\324=\203w \325\262\311	!\210)r\321	!q\210\326\327\n\330\"\206\211 d!\305\331\"\211\203\226 \211\202\346 \332\333T\330\"\206\240 e\327\330\"\206\250 d\"\305\334\"\322\203\331 \305\331\"\204\331 \332\333T\330\"\206\303 e\327\330\"\206\313 d\"B\262\305\334\"\262\202\256 \305\331\"B\262B\266\203\262\262)\322A@@AA\3351\276\336!\210\337!\211\203\211@\340\"A\262A\266\202\202 \210\322\325\341\203D\203D@\203-\322\262\2028\211@\232\2038\211\262\210\211T\262A\262\202\266\211\262\203Q\211\202S\211A\262\342!\211\343\267\202\216\321!\344!B\202\217\345!\346!B\202\217\322B\202\217\305\341\347@#\211\205\211\321!\344!B\262\202\217\322\262\262\211A\262\242\350\206\243\312!!\210\211b\210\351=\205\266\352\353!\205\266\353 \266\203\2620\202\330	?\205\326\354\355\356\357\360\361#\362\363\356\360\364###\262\266\211\203\353r\321	!q\210\365	!\210)\210\205\363\366 \266\203\202\276\305\347\"\211\203\224\203'\211\205\f\312!\262\211\205\313!\262\211\203\"\314!\210\202&\315\316!\210\210\317!\210\344!\2036\211b\202\232r\321!q\210\305\331\"\211\203J\211\202\235\332\333T\330\"\206Ue\327	\330\"\206^d\"\305\334\"\322\203\220\305\331\"\204\220\332\333T\330\"\206ze\327\330\"\206\202d\"B\262\305\334\"\262\202e\305\331\"B\262B\266\203\262)\325A@@AA\3671s\336!\210\337!\211\203\311\211@\340\"A\262A\266\202\202\265\210\322\325\341\203\371\203\371@\203\342\322\262\202\355\211@\232\203\355\211\262\210\211T\262A\262\202\316\266\211\262\203\211\202\211A\262\342!\211\370\267\202C\321!\344!B\202D\345!\346!B\202D\322B\202D\305\341\347@#\211\205>\321!\344!B\262\202D\322\262\262\211A\262\242\350\206X\312!!\210\211b\210\351=\205k\352\353!\205k\353 \266\203\2620\202\215	?\205\213\354\355\356\357\360\361#\362\363\356\360\364###\262\266\203\266\202\202\232\311\"\262\202\276\305\323\"\211\371\267\202\273\365\"\202\274\311\"\202\274\322\262\207" [major-mode treemacs--no-messages treemacs--nearest-path f-ext #s(hash-table size 2 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data ("py" 12 "org" 504)) get-text-property :index s-ends-with\? " definition*" treemacs--expand-tag-node get-file-buffer get-buffer-window select-window other-window 1 find-file buffer-live-p marker-buffer nil :state tag-node-closed t next-button next-single-property-change button :path buffer-substring-no-properties previous-single-property-change :parent (error) find-file-noselect treemacs--get-imenu-index assoc 0 type-of #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 350 overlay 360 integer 370 cons 376)) marker-position overlay-buffer overlay-start org-imenu-marker switch-to-buffer org-mode fboundp org-reveal message "%s %s" propertize "[Treemacs]" face font-lock-keyword-face format "Something went wrong when finding tag '%s': %s" treemacs-tags-face treemacs--collapse-tag-node treemacs-select-window (error) #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 787 overlay 797 integer 807 cons 813)) #s(hash-table size 2 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (tag-node-open 937 tag-node-closed 946))] 24 (#$ . 10615)])
#@116 Open tags node items for BTN.
Open all tag section under BTN when call is RECURSIVE.

(fn BTN &optional RECURSIVE)
(defalias 'treemacs--expand-tag-node #[513 "\306\307\"\212`\310\311\312T\313\"\206 e\314\313\"\206 d\315\316$\210\317 \210\212	G\320\321 \322\"\312T\313\"\2063 e\262Zb\210	c\210\323!\266)\314\313\"\206J db\210\324\325\306\326\"T\327\nGY\204j @\f=\203j A=\204n \330!\210\nHP@\3101P2P\203\353 	\211\203\352 \211@\331!\203\255 \332@\313\333\334\335\336\337\340\310\315\341\342\326\307A&D\202\310 \332@\313\343\334\335\336\337\340\310\315\344\342\326\345A&D\346\203\341 @\211B\262\210\211T\262A\262\202\311 \266A\266\202\202\203 \210\237\266\206\"c\210\347\306\350\"\211\203\377 \211\202R\351\312T\313\"\206\ne\314\313\"\206d\"\306\342\"\310\203E\306\350\"\204E\351\312T\313\"\206/e\314\313\"\2067d\"B\262\306\342\"\262\202\306\350\"B\262B\266\203\262\306\342\"\306\315\"\211\352\267\202\314\306\350\"\202\323\306\350\"\211\203w\211\202\307\351\312T\313\"\206\201e\314\313\"\206\211d\"\306\342\"\310\203\272\306\350\"\204\272\351\312T\313\"\206\244e\314\313\"\206\254d\"B\262\306\342\"\262\202\217\306\350\"B\262B\266\203\262\202\323\211\353\354\"\262\262\262#\210\203\355!\346\203\f@\306\315\"\341=\203 \312T\313\"\206\371eb\210\356\322\"\210\210\211T\262A\262\202\341\266\202\357!\210)\360`\"\262)\207" [buffer-read-only treemacs-icon-tag-node-open treemacs--indentation-string-cache treemacs--indentation-string-cache-key treemacs-indentation treemacs-indentation-string get-text-property :index nil put-text-property previous-single-property-change button next-single-property-change :state tag-node-open beginning-of-line next-button point-at-bol t delete-char apply concat :depth "\n" treemacs--build-indentation-cache imenu--subalist-p propertize (t) category default-button face treemacs-tags-face help-echo tag-node-closed :parent (t) tag-node :marker 0 treemacs-on-expand :path buffer-substring-no-properties #s(hash-table size 2 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (file-node-open 356 tag-node-open 363)) error "Impossible state of parent: %s" treemacs--get-children-of treemacs--expand-tag-node treemacs--reopen-tags-under count-lines treemacs-icon-tag-leaf treemacs-icon-tag-node-closed] 33 (#$ . 15102)])
#@186 Recursively close tag section BTN.
Workaround for tag section having no easy way to purge all open tags below a
button from cache. Easiest way is to just do it manually here.

(fn BTN)
(defalias 'treemacs--collapse-tag-node-recursive #[257 "\300!\301\2032 @\302\303\"\304=\203& \305!\210\306T\307\"\206  eb\210\310!\210\210\211T\262A\262\202 \266\306T\307\"\206= eb\210\310!\207" [treemacs--get-children-of 0 get-text-property :state tag-node-open treemacs--collapse-tag-node-recursive previous-single-property-change button treemacs--collapse-tag-node] 7 (#$ . 17517)])
#@109 Close tags node at BTN.
Remove all open tag entries under BTN when RECURSIVE.

(fn BTN &optional RECURSIVE)
(defalias 'treemacs--collapse-tag-node #[513 "\211\203 \302!\207\212\303\212	G\304\305 \306\"\307T\310\"\206 e\262Zb\210	c\210\311!\266)\312\307T\310\"\2065 e\313\310\"\206= d\314\315$\210\304\316 !\211\203\\ \317\320\"T\321\306\"\317\320\"\262U\204b \322 \210\202\277 \313\310\"\206j d\205\235 \317\320\"\304\313\310\"\206| d\306\"\211\203\233 \317\320\"W\203\233 \304\313\310\"\206\224 d\306\"\262\202~ \262\211\203\271 \323\307T\310\"\206\253 e!\313\310\"\206\264 d\262\202\272 d|\266\210\324\317\325\"\211\203\315 \211\202\326\307T\310\"\206\327 e\313\310\"\206\340 d\"\317\327\"\303\203\317\325\"\204\326\307T\310\"\206\373 e\313\310\"\206d\"B\262\317\327\"\262\202\346 \317\325\"B\262B\266\203\262!*\207" [buffer-read-only treemacs-icon-tag-node-closed treemacs--collapse-tag-node-recursive nil next-button point-at-bol t previous-single-property-change button delete-char put-text-property next-single-property-change :state tag-node-closed point-at-eol get-text-property :depth copy-marker delete-trailing-whitespace previous-button treemacs-on-collapse :path buffer-substring-no-properties :parent] 12 (#$ . 18109)])
#@659 Extract a tag's buffer and position stored in ITEM.
The position can be stored in the following ways:

* ITEM is a marker pointing to a tag provided by imenu.
* ITEM is an overlay pointing to a tag provided by imenu with semantic mode.
* ITEM is a raw number pointing to a buffer position.
* ITEM is a cons: special case for imenu elements of an `org-mode' buffer.
  ITEM is an imenu subtree and the position is stored as a marker in the first
  element's 'org-imenu-marker text property.

Either way the return value is a const consisting of the buffer and the position
of the tag. They might also be nil if the pointed-to buffer does not exist.

(fn ITEM)
(defalias 'treemacs--extract-position #[257 "\300!\211\301\267\2029 \302!\303!B\202: \304!\305!B\202: \306B\202: \307\310\311@#\211\2054 \302!\303!B\262\202: \306\207" [type-of #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 9 overlay 19 integer 29 cons 35)) marker-buffer marker-position overlay-buffer overlay-start nil get-text-property 0 org-imenu-marker] 6 (#$ . 19408)])
(byte-code "\300\301\302\303#\300\301\304\305#\300\207" [function-put treemacs--extract-position compiler-macro treemacs--extract-position--inliner side-effect-free t] 5)
#@26 

(fn INLINE--FORM ITEM)
(defalias 'treemacs--extract-position--inliner #[514 "\3002j \211\301!\203 \211\202 \302\303!\304\305D\306\307D\310\311D\312DED\306\313D\310\314D\315DED\306\316D\310\317ED\306\310D\320\321\322\323\306\324D\325\fDFD\310\311\321D\312\321DEED\257=\203` \211\202g \326DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "item" pcase type-of quote marker cons marker-buffer marker-position overlay overlay-buffer overlay-start integer nil -when-let org-marker get-text-property 0 org-imenu-marker car macroexp-let*] 17 (#$ . 20675)])
#@337 Call the imenu index of the tag at TAG-PATH and go to its position.
ORG? should be t when this function is called for an org buffer and index since
org requires a slightly different position extraction because the position of a
headline with subelements is saved in an 'org-imenu-marker' text property.

(fn TAG-PATH &optional ORG\=\?)
(defalias 'treemacs--call-imenu-and-goto-tag #[513 "A@@AA\3021\322 \303!\210\304!\211\203( \211@\305\"A\262A\266\202\202 \210\306\307\310\203X \203X @\203A \306\262\202L \211@\232\203L \211\262\210\211T\262A\262\202- \266\211\262\203e \211\202g \211A\262\311!\211\312\267\202\242 \313!\314!B\202\243 \315!\316!B\202\243 \306B\202\243 \317\310\320@#\211\205\235 \313!\314!B\262\202\243 \306\262\262\211A\262\242\321\206\267 \322!!\210\211b\210\323=\205\312 \324\325!\205\312 \325 \266\203\2620\202\354 	?\205\352 \326\327\330\331\332\333#\334\335\330\332\336###\262\207" [major-mode treemacs--no-messages (error) find-file-noselect treemacs--get-imenu-index assoc nil t 0 type-of #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 114 overlay 124 integer 134 cons 140)) marker-buffer marker-position overlay-buffer overlay-start get-text-property org-imenu-marker switch-to-buffer get-file-buffer org-mode fboundp org-reveal message "%s %s" propertize "[Treemacs]" face font-lock-keyword-face format "Something went wrong when finding tag '%s': %s" treemacs-tags-face] 15 (#$ . 21273)])
#@33 Go to the tag at BTN.

(fn BTN)
(defalias 'treemacs--goto-tag #[257 "r\304!q\210\305\306\"\307!\211\310\267\202C \304!\311!B\202D \312!\313!B\202D \314B\202D \305\315\316@#\211\205> \304!\311!B\262\202D \314\262\262)\211A\262\242\203n \317\314\320#\210\211b\210\321=\205M\322\323!\205M\323 \202M	\324\267\202I\325r\304!q\210\305\326\"\211\203\207 \211\202\332 \327\330T\331\"\206\222 e\332\331\"\206\233 d\"\305\333\"\314\203\315 \305\326\"\204\315 \327\330T\331\"\206\267 e\332\331\"\206\277 d\"B\262\305\333\"\262\202\242 \305\326\"B\262B\266\203\262)!\202M\334r\304!q\210\327\330T\331\"\206\363 e\332\331\"\206\374 d\")!\202M\335\336r\304!q\210\327\330T\331\"\206e\332\331\"\206d\")\337\340#D\211\203=\n\204=\341\342\336\343\337\344#\345\346\345\"!#\210\205D\347\350!\262\202M\351\352	\"\207" [major-mode treemacs-goto-tag-strategy treemacs--no-messages treemacs-pulse-on-failure marker-buffer get-text-property :marker type-of #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 19 overlay 29 integer 39 cons 45)) marker-position overlay-buffer overlay-start nil 0 org-imenu-marker switch-to-buffer t org-mode fboundp org-reveal #s(hash-table size 3 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (refetch-index 116 call-xref 225 issue-warning 258)) #[513 "A@@AA\3021\322 \303!\210\304!\211\203( \211@\305\"A\262A\266\202\202 \210\306\307\310\203X \203X @\203A \306\262\202L \211@\232\203L \211\262\210\211T\262A\262\202- \266\211\262\203e \211\202g \211A\262\311!\211\312\267\202\242 \313!\314!B\202\243 \315!\316!B\202\243 \306B\202\243 \317\310\320@#\211\205\235 \313!\314!B\262\202\243 \306\262\262\211A\262\242\321\206\267 \322!!\210\211b\210\323=\205\312 \324\325!\205\312 \325 \266\203\2620\202\354 	?\205\352 \326\327\330\331\332\333#\334\335\330\332\336###\262\207" [major-mode treemacs--no-messages (error) find-file-noselect treemacs--get-imenu-index assoc nil t 0 type-of #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (marker 114 overlay 124 integer 134 cons 140)) marker-buffer marker-position overlay-buffer overlay-start get-text-property org-imenu-marker switch-to-buffer get-file-buffer org-mode fboundp org-reveal message #1="%s %s" propertize #2="[Treemacs]" face font-lock-keyword-face format "Something went wrong when finding tag '%s': %s" treemacs-tags-face] 15 "Call the imenu index of the tag at TAG-PATH and go to its position.\nORG? should be t when this function is called for an org buffer and index since\norg requires a slightly different position extraction because the position of a\nheadline with subelements is saved in an 'org-imenu-marker' text property.\n\n(fn TAG-PATH &optional ORG\\=\\?)"] :path buffer-substring-no-properties previous-single-property-change button next-single-property-change :parent xref-find-definitions "Tag '%s' is located in a buffer that does not exist." propertize face treemacs-tags-face message #1# #2# font-lock-keyword-face format apply treemacs--do-pulse treemacs-on-failure-pulse-face error "[Treemacs] '%s' is an invalid value for treemacs-goto-tag-strategy"] 14 (#$ . 22795)])
#@185 Goto tag given by TAG-PATH.
Will return the found tag node, or nil if no such node exists (anymore). In this
case point will be left at the next highest node available.

(fn TAG-PATH)
(defalias 'treemacs--goto-tag-button-at #[257 "\3002)\211\211A\262\242\211A\262\242\301\302\"\211\205' \303 \210\304 \210\305\306 `\"\210\211\262\211\205$\307\310\"\311=\203E \312T\313\"\206? eb\210\314!\210\211\203\305 \211@\302\315!\316\317\203\216 \203\216 @\203f \302\262\202\202 \320\312T\313\"\206p e\321\313\"\206x d\"\230\203\202 \211\262\210\211T\262A\262\202R \266\211\262\211\203\265 \211\262\307\310\"\322=\203\275 \312T\313\"\206\254 eb\210\323!\210\202\275 b\210\324\300\302\"\210\210A\266\202\202F \210\302\315!\316\317\203\203@\203\340 \302\262\202\374 \320\312T\313\"\206\352 e\321\313\"\206\362 d\"	\230\203\374 \211\262\210\211T\262A\262\202\314 \266\211\262\211\203\211b\210\325\316\"\202\"b\210\324\300\302\"\262\262\266\2040\207" [--cl-block-treemacs--goto-tag-button-at-- treemacs-find-file-node nil treemacs--evade-image hl-line-highlight set-window-point get-buffer-window get-text-property :state file-node-closed previous-single-property-change button treemacs--expand-file-node treemacs--get-children-of t 0 buffer-substring-no-properties next-single-property-change tag-node-closed treemacs--expand-tag-node throw copy-marker] 18 (#$ . 26077)])
#@54 Reopen previously openeded tags under BTN.

(fn BTN)
(defalias 'treemacs--reopen-tags-under #[257 "\212\304\305\"\211\203 \211\202] \306\307T\310\"\206 e\311\310\"\206 d\"\304\312\"\313\203P \304\305\"\204P \306\307T\310\"\206: e\311\310\"\206B d\"B\262\304\312\"\262\202% \304\305\"B\262B\266\203\262\313\314\n	#+\315\316\317H\"\320!\211\205E\211@\313\321\322\203\203@\203\223 \313\262\202\373 \323H\304\305\"\211\203\242 \211\202\362 \306\307T\310\"\206\254 e\311\310\"\206\264 d\"\304\312\"\313\203\345 \304\305\"\204\345 \306\307T\310\"\206\317 e\311\310\"\206\327 d\"B\262\304\312\"\262\202\272 \304\305\"B\262B\266\203\262\232\203\373 \211\262\210\211T\262A\262\202 \266\211\262\211\203+\304\324\"\325=\203=\307T\310\"\206\"eb\210\326!\210\202=\211\317\327\n\317H\"I\266\330\331\"\210\210A\266\202\202u \262\266\204)\207" [treemacs-dom default key table get-text-property :path buffer-substring-no-properties previous-single-property-change button next-single-property-change :parent nil gethash -reject treemacs-dom-node->closed 3 treemacs--get-children-of t 0 1 :state tag-node-closed treemacs--expand-tag-node delete treemacs--do-for-all-child-nodes treemacs-dom-node->remove-from-dom!] 22 (#$ . 27502)])
(provide 'treemacs-tags)
